const Service = require('node-windows').Service;
const path = require('path');

// Create a new service object
var svc = new Service({
  name:'has',
  description: 'Home Automation Service',
  script: path.join(__dirname, '../src/index.js')
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();