//********************************************************************
// Name: database/index.js
// Desc: Configuration dabatase using files.
// Auth: Franco Rosatti
// Date: Aug-2020
// Vers: 1.0
//********************************************************************

const fs = require('fs');
const path = require('path');

//PRIVATE
const filename_outputs =  path.join(__dirname, 'outputs.json');
const filename_inputs =  path.join(__dirname, 'inputs.json');

const load = (filename) => {
    try {
        if (fs.existsSync(filename)) {
            return JSON.parse(fs.readFileSync(filename));
        } else {
            return [];
        }
    } catch (e) {
        return [];
    }
}

const save = (filename, values) => {
    fs.writeFileSync(filename, JSON.stringify(values))
}

const loadOutputs = () => {
    return load(filename_outputs);
}

const saveOutputs = (values) => {
    save(filename_outputs, values)
}

const loadInputs = () => {
    return load(filename_inputs);
}

const saveInputs = (values) => {
    save(filename_inputs, values)
}

//PUBLIC
const listOutputs = () => {
    return loadOutputs();
}

const readOutput = (id) => {
    const outputs = loadOutputs();
    const output = outputs.find(output => output.id === id);
    if(output){
        return output;
    }
    return 'Output not found';
}

const writeOutput = (id, status) => {
    const outputs = loadOutputs();
    const output = outputs.find(output => output.id === id);
    if(output){
        output.status = status;
        saveOutputs(outputs);
        return output;
    }
    return 'Output not found';
}

const listInputs = () => {
    return loadInputs();
}

const readInput = (id) => {
    const inputs = loadInputs();
    const input = inputs.find(input => input.id === id);
    if(input){
        return input;
    }
    return 'Input not found';
}

//TODO: Add Rules

//EXPORT
module.exports = { listOutputs, readOutput, writeOutput, listInputs, readInput };