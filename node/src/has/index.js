//********************************************************************
// Name: has/index.js
// Desc: Home Automation Service. Apply programmed rules, interface with hardware
// Auth: Franco Rosatti
// Date: Aug-2020
// Vers: 1.0
//********************************************************************

const db = require('../database/index.js');

//PRIVATE
let status = {
    count: 0,
    state: 'stopped'
};

const _main = () => {
    if(status.state == 'running'){
        status.count++;
        //TODO: Main logic
    }
}

//PUBLIC
const main = (interval = 100) => {
    setInterval(_main, interval);
}

const getStatus = () => status;

const start = () => {
    status.count = 0;
    status.state = 'running';
}

const stop = () => {
    status.state = 'stopped';
}

module.exports = { main, start, stop, getStatus }