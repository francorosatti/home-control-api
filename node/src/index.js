//********************************************************************
// Name: index.js
// Desc: Main code of node app
// Auth: Franco Rosatti
// Date: Aug-2020
// Vers: 1.0
//********************************************************************

const path = require('path');

const morgan = require('morgan');
const express = require('express');

const has = require('./has/index.js');

const app = express();

//settings
app.set('port', process.env.PORT || 4000);

//middlewares
app.use(morgan('combined'));

//routes
app.use(require('./routes/index.js'));

//main
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});

has.main();