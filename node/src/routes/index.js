//********************************************************************
// Name: routes/index.js
// Desc: Routes definition for API
// Auth: Franco Rosatti
// Date: Aug-2020
// Vers: 1.0
//********************************************************************

const express = require('express');

const db = require('../database/index.js');
const auth = require('../security/index.js');
const has = require('../has/index.js');

const router = express.Router();

//*************************************************************
// OUTPUTS
//*************************************************************
router.get('/outputs', (req, res) => {
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    res.json(db.listOutputs());
});

router.get('/outputs/:id', (req, res) => {
    const id = req.params.id;
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    res.json(db.readOutput(id));
});

router.get('/outputs/:id/set', (req, res) => {
    const id = req.params.id;
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    res.json(db.writeOutput(id, true));
});

router.get('/outputs/:id/reset', (req, res) => {
    const id = req.params.id;
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    res.json(db.writeOutput(id, false));
});

//*************************************************************
// INPUTS
//*************************************************************
router.get('/inputs', (req, res) => {
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    res.json(db.listInputs());
});

router.get('/inputs/:id', (req, res) => {
    const id = req.params.id;
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    res.json(db.readInput(id));
});

//*************************************************************
// RULES
//*************************************************************
//TODO: Add rules endpoints

//*************************************************************
// HAS
//*************************************************************
router.get('/has', (req, res) => {
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    res.json(has.getStatus());
});

//TODO: Change to POST
router.get('/has/start', (req, res) => {
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    has.start();
    res.json(has.getStatus());
});

//TODO: Change to POST
router.get('/has/stop', (req, res) => {
    const email = req.query.email;
    const apikey = req.query.apikey;
    if (!auth.auth(email, apikey)) {
        res.sendStatus(401);
        return;
    }

    has.stop();
    res.json(has.getStatus());
});

module.exports = router;