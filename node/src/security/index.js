//********************************************************************
// Name: security/index.js
// Desc: Auth functions
// Auth: Franco Rosatti
// Date: Aug-2020
// Vers: 1.0
//********************************************************************

//PRIVATE

//PUBLIC
function auth(email, apikey) {
    return true; //DEBUG
    if (email == null || email == undefined || email == "" || email.length <= 0) return false;
    if (apikey == null || apikey == undefined || apikey == "" || apikey.length <= 0) return false;
    return true;
}

module.exports = { auth }