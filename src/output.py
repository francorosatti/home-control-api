###############################################################################
# Name: output.py
# Desc: Contiene entidades y servicios relacionados con las salidas
# Auth: Franco Rosatti
# Date: Aug-2019
# Vers: 1.0
###############################################################################

from flask import make_response, abort
# import rpi
import winsound


OUTPUTS = {
            '1': {
                'id': 1,
                'name': 'Led#01',
                'status': True
            },
            '2': {
                'id': 2,
                'name': 'Rele#01',
                'status': False
            }
}


def getOutputs():
    return [OUTPUTS[key] for key in sorted(OUTPUTS.keys())]


def getOutputStatus(outputId):
    for o in OUTPUTS.values():
        if o['id'] == outputId:
            return o['status']

    abort(404, "Output not found")


def setOutputStatus(outputId, status):
    for o in OUTPUTS.values():
        if o['id'] == outputId:
            o['status'] = status
            if o['id'] == 1:
                if status == 0:
                    # rpi.ledOff()
                    winsound.Beep(440, 500)
                else:
                    # rpi.ledOff()
                    winsound.Beep(720, 500)
            elif o['id'] == 2:
                if status == 0:
                    # rpi.releOff()
                    winsound.Beep(1000, 500)
                else:
                    # rpi.releOn()
                    winsound.Beep(1500, 500)
            return make_response("Output status successfully updated", 200)

    abort(404, "Output not found")
