###############################################################################
# Name: rpi.py
# Desc: Contiene servicios relacionados con el manejo del HW de la RPi
# Auth: Franco Rosatti
# Date: Aug-2019
# Vers: 1.0
###############################################################################

import RPi.GPIO as GPIO

GPIO_LED = 11
GPIO_RELE = 13


def init():
    print("Initiating...")
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(GPIO_LED, GPIO.OUT)
    GPIO.setup(GPIO_RELE, GPIO.OUT)


def deinit():
    GPIO.cleanup()


def ledOn():
    GPIO.output(GPIO_LED, GPIO.HIGH)


def ledOff():
    GPIO.output(GPIO_LED, GPIO.LOW)


def releOn():
    GPIO.output(GPIO_RELE, GPIO.HIGH)


def releOff():
    GPIO.output(GPIO_LED, GPIO.LOW)
